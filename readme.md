# Docker Demo

This is a short demo of docker in conjunction with WordPress & MySQL.

## What is Docker

Platform for developers and sysadmins to develop, deploy, and run applications with containers. Containers are runtime instances of images (what happens during an application is executing). Images are everything required to run a piece of code.

[Documentation on Containers](https://www.docker.com/resources/what-container)

[General Documentation on Docker](https://docs.docker.com/)

### Benefits of Docker

* Flexible
* Lightweight
* Interchangable
* Portable
* Scalable
* Stackable

### Install Docker

_Create an account at Docker_

In order to download docker, you first need to create an account. This is also useful for the Docker store to get other images

[Signup for Docker](https://store.docker.com/signup)

_Once you have an account, install Docker on your machine:_

[Install Docker](https://store.docker.com/editions/community/docker-ce-desktop-mac)

### Running Docker

Once you have copied over the repo to your local machine, explore the docker-compose.yml. It has three defined images in it, along with comments about all three

1. mariadb -> official MariaDB image from Docker
2. wordpress -> official WordPress image from Docker
3. phpmyadmin -> official phpMyAdmin image from Docker

To start up docker, run the following command in the terminal

`
docker-compose up -d
`

_up_ starts container with images defined in docker-compose.yml.

_-d_ flag means to run docker in detached mode. Runs the application in the background v. forground/attached. More on that here:  
  
[Docker Run Reference](https://docs.docker.com/engine/reference/run/#general-form)  
  
[Docker Detach for beginners](https://medium.freecodecamp.org/dockers-detached-mode-for-beginners-c53095193ee9)

  
To stop the container:
  
`
docker-compose stop
`
  
  
To view the logs of the container while its running:  
  
`
docker-compose logs
`

  
To view information about the container running and all images withing it:
  
`
docker ps
`